import { AppShell, Button } from "@mantine/core";
import { Link, Outlet, useLocation, useRouteError } from "react-router-dom";

function App() {
  const location = useLocation();
  const routeError = useRouteError();
  console.log("routeError", routeError);
  console.log("Current path:", location.pathname);
  console.log("Full URL:", window.location.href);

  return (
    <>
      <AppShell header={{ height: 60 }} padding="lg">
        <AppShell.Header>
          <div
            style={{ display: "flex", justifyContent: "center", gap: "1rem" }}
          >
            <Link to="/">
              <Button>Home</Button>
            </Link>
            <Link to="/services">
              <Button>Services</Button>
            </Link>
            <Link to="/about">
              <Button>About</Button>
            </Link>
          </div>
        </AppShell.Header>

        <AppShell.Main>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Outlet />
          </div>
        </AppShell.Main>
      </AppShell>
    </>
  );
}

export default App;
