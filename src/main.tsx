import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { MantineProvider, createTheme } from "@mantine/core";
import "@mantine/core/styles.css";

const theme = createTheme({
  /** Your theme override here */
});

export const appRoutes = createBrowserRouter(
  [
    {
      path: "/",
      element: <App />,
      errorElement: <App />,
      children: [
        {
          path: "",
          element: <div>Home</div>,
        },
        {
          path: "/services",
          element: <div>Services</div>,
        },
        {
          path: "/about",
          element: <div>About</div>,
        },
      ],
    },
  ],
  {
    basename: "/gitlab-pages-router",
  }
);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <MantineProvider theme={theme}>
      <RouterProvider router={appRoutes} />
    </MantineProvider>
  </React.StrictMode>
);
